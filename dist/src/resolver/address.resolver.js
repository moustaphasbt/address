"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const base_resolver_1 = require("../common/base.resolver");
const dto_1 = require("../dto");
const address_entity_1 = require("../entity/address.entity");
const address_service_1 = require("../service/address.service");
let AddressResolver = class AddressResolver extends base_resolver_1.BaseResolver(address_entity_1.Address, dto_1.CreateAddressInput, dto_1.UpdateAddressInput, address_service_1.AddressService) {
};
AddressResolver = __decorate([
    graphql_1.Resolver(() => address_entity_1.Address)
], AddressResolver);
exports.AddressResolver = AddressResolver;
//# sourceMappingURL=address.resolver.js.map