import { AddressModule } from './src/address.module';
import { Address } from './src/entity/address.entity';
import { City } from './src/entity/city.entity';
import { District } from './src/entity/district.entity';
import { Region } from './src/entity/region.entity';
import { AddressResolver } from './src/resolver/address.resolver';
import { CityResolver } from './src/resolver/city.resolver';
import { DistrictResolver } from './src/resolver/district.resolver';
import { RegionResolver } from './src/resolver/region.resolver';
import { AddressService } from './src/service/address.service';
import { CityService } from './src/service/city.service';
import { DistrictService } from './src/service/district.service';
import { RegionService } from './src/service/region.service';

export {
  AddressModule,
  Region,
  District,
  City,
  Address,
  AddressService,
  CityService,
  DistrictService,
  RegionService,
  RegionResolver,
  DistrictResolver,
  CityResolver,
  AddressResolver,
};
