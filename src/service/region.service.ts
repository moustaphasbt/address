import { Injectable } from '@nestjs/common';
import { BaseService } from '../common/base.service';
import { Region } from '../entity/region.entity';

@Injectable()
export class RegionService extends BaseService(Region) {}
