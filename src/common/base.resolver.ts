import { Inject, Type } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { DeepPartial } from 'typeorm';
import { BaseServiceInterface } from './base.service';

export function BaseResolver<
  E extends Type<unknown>,
  C extends Type<unknown>,
  U extends Type<unknown>,
  S extends Type<unknown>
>(entityRef: E, createInputRef: C, updateInputRef: U, serviceRef: S): any {
  @Resolver({ isAbstract: true })
  abstract class BaseResolverHost {
    constructor(
      @Inject(serviceRef) private readonly service: BaseServiceInterface<E>,
    ) {}

    @Query(() => [entityRef], {
      name: `findAll${entityRef.name}`,
      nullable: true,
    })
    async findAll(): Promise<E[]> {
      return this.service.findAll();
    }

    @Query(() => entityRef, {
      name: `findOne${entityRef.name}`,
      nullable: true,
    })
    async findOne(@Args('id', { type: () => String }) id: string) {
      return this.service.findOne(id);
    }

    @Query(() => entityRef, {
      name: `findOneOrFail${entityRef.name}`,
    })
    async findOneOrFail(@Args('id', { type: () => String }) id: string) {
      return this.service.findOneOrFail(id);
    }

    @Mutation(() => entityRef, { name: `create${entityRef.name}` })
    async create(
      @Args('input', { type: () => createInputRef })
      input: DeepPartial<E>,
    ) {
      return this.service.save(input);
    }

    @Mutation(() => entityRef, { name: `update${entityRef.name}` })
    async update(
      @Args('id', { type: () => String }) id: string,
      @Args('input', { type: () => updateInputRef })
      input: DeepPartial<E>,
    ) {
      return this.service.update(id, input);
    }
  }
  return BaseResolverHost;
}
