"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAddressInput = exports.CreateAddressInput = exports.UpdateDistrictInput = exports.CreateDistrictInput = exports.DistrictIdInput = exports.UpdateCityInput = exports.CreateCityInput = exports.CityIdInput = exports.UpdateRegionInput = exports.CreateRegionInput = exports.RegionIdInput = void 0;
const graphql_1 = require("@nestjs/graphql");
const class_validator_1 = require("class-validator");
let RegionIdInput = class RegionIdInput {
};
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], RegionIdInput.prototype, "id", void 0);
RegionIdInput = __decorate([
    graphql_1.InputType()
], RegionIdInput);
exports.RegionIdInput = RegionIdInput;
let CreateRegionInput = class CreateRegionInput {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateRegionInput.prototype, "name", void 0);
CreateRegionInput = __decorate([
    graphql_1.InputType()
], CreateRegionInput);
exports.CreateRegionInput = CreateRegionInput;
let UpdateRegionInput = class UpdateRegionInput {
};
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    __metadata("design:type", String)
], UpdateRegionInput.prototype, "name", void 0);
UpdateRegionInput = __decorate([
    graphql_1.InputType()
], UpdateRegionInput);
exports.UpdateRegionInput = UpdateRegionInput;
let CityIdInput = class CityIdInput {
};
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CityIdInput.prototype, "id", void 0);
CityIdInput = __decorate([
    graphql_1.InputType()
], CityIdInput);
exports.CityIdInput = CityIdInput;
let CreateCityInput = class CreateCityInput {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateCityInput.prototype, "name", void 0);
__decorate([
    graphql_1.Field(() => RegionIdInput),
    __metadata("design:type", RegionIdInput)
], CreateCityInput.prototype, "region", void 0);
CreateCityInput = __decorate([
    graphql_1.InputType()
], CreateCityInput);
exports.CreateCityInput = CreateCityInput;
let UpdateCityInput = class UpdateCityInput {
};
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    __metadata("design:type", String)
], UpdateCityInput.prototype, "name", void 0);
__decorate([
    graphql_1.Field(() => RegionIdInput, { nullable: true }),
    __metadata("design:type", RegionIdInput)
], UpdateCityInput.prototype, "region", void 0);
UpdateCityInput = __decorate([
    graphql_1.InputType()
], UpdateCityInput);
exports.UpdateCityInput = UpdateCityInput;
let DistrictIdInput = class DistrictIdInput {
};
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], DistrictIdInput.prototype, "id", void 0);
DistrictIdInput = __decorate([
    graphql_1.InputType()
], DistrictIdInput);
exports.DistrictIdInput = DistrictIdInput;
let CreateDistrictInput = class CreateDistrictInput {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateDistrictInput.prototype, "name", void 0);
__decorate([
    graphql_1.Field(() => DistrictIdInput, { nullable: true }),
    __metadata("design:type", DistrictIdInput)
], CreateDistrictInput.prototype, "parent", void 0);
__decorate([
    graphql_1.Field(() => CityIdInput),
    __metadata("design:type", CityIdInput)
], CreateDistrictInput.prototype, "city", void 0);
CreateDistrictInput = __decorate([
    graphql_1.InputType()
], CreateDistrictInput);
exports.CreateDistrictInput = CreateDistrictInput;
let UpdateDistrictInput = class UpdateDistrictInput {
};
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    __metadata("design:type", String)
], UpdateDistrictInput.prototype, "name", void 0);
__decorate([
    graphql_1.Field(() => DistrictIdInput, { nullable: true }),
    __metadata("design:type", DistrictIdInput)
], UpdateDistrictInput.prototype, "parent", void 0);
__decorate([
    graphql_1.Field(() => CityIdInput, { nullable: true }),
    __metadata("design:type", CityIdInput)
], UpdateDistrictInput.prototype, "city", void 0);
UpdateDistrictInput = __decorate([
    graphql_1.InputType()
], UpdateDistrictInput);
exports.UpdateDistrictInput = UpdateDistrictInput;
let CreateAddressInput = class CreateAddressInput {
};
__decorate([
    graphql_1.Field(() => Number, { nullable: true }),
    __metadata("design:type", Number)
], CreateAddressInput.prototype, "number", void 0);
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateAddressInput.prototype, "street", void 0);
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    __metadata("design:type", String)
], CreateAddressInput.prototype, "comment", void 0);
__decorate([
    graphql_1.Field(() => DistrictIdInput),
    __metadata("design:type", DistrictIdInput)
], CreateAddressInput.prototype, "district", void 0);
CreateAddressInput = __decorate([
    graphql_1.InputType()
], CreateAddressInput);
exports.CreateAddressInput = CreateAddressInput;
let UpdateAddressInput = class UpdateAddressInput {
};
__decorate([
    graphql_1.Field(() => Number, { nullable: true }),
    __metadata("design:type", Number)
], UpdateAddressInput.prototype, "number", void 0);
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    __metadata("design:type", String)
], UpdateAddressInput.prototype, "street", void 0);
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    __metadata("design:type", String)
], UpdateAddressInput.prototype, "comment", void 0);
__decorate([
    graphql_1.Field(() => DistrictIdInput, { nullable: true }),
    __metadata("design:type", DistrictIdInput)
], UpdateAddressInput.prototype, "district", void 0);
UpdateAddressInput = __decorate([
    graphql_1.InputType()
], UpdateAddressInput);
exports.UpdateAddressInput = UpdateAddressInput;
//# sourceMappingURL=dto.js.map