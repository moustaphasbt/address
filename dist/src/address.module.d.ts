import { DynamicModule } from '@nestjs/common';
import { AddressOptions } from './utils/common';
export declare class AddressModule {
    static forRoot({ imports }: Pick<AddressOptions, 'imports'>): DynamicModule;
}
