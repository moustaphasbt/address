import { Type } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';

export interface BaseServiceInterface<T extends Type<unknown>> {
  findOne(id: string): Promise<T>;
  findOneOrFail(id: string): Promise<T>;
  findAll(): Promise<T[]>;
  save(input: DeepPartial<T>): Promise<T>;
  update(id: string, input: DeepPartial<T>): Promise<T>;
}

export function BaseService<E extends Type<unknown>>(entityRef: E): any {
  abstract class BaseServiceHost<T extends Type<unknown>>
    implements BaseServiceInterface<T> {
    constructor(
      @InjectRepository(entityRef)
      private readonly repository: Repository<T>,
    ) {}

    async findOne(id: string): Promise<T> {
      return this.repository.findOne(id);
    }

    async findOneOrFail(id: string): Promise<T> {
      return this.repository.findOneOrFail(id);
    }

    async findAll(): Promise<T[]> {
      return this.repository.find();
    }

    async save(input: DeepPartial<T>): Promise<T> {
      return this.repository.save(input);
    }

    async update(id: string, input: DeepPartial<T>): Promise<T> {
      return this.repository.save({ id, ...input });
    }
  }
  return BaseServiceHost;
}
