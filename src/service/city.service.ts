import { Injectable } from '@nestjs/common';
import { BaseService } from '../common/base.service';
import { City } from '../entity/city.entity';

@Injectable()
export class CityService extends BaseService(City) {}
