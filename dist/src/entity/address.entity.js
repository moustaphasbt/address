"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
const graphql_1 = require("@nestjs/graphql");
const typeorm_1 = require("typeorm");
const city_entity_1 = require("./city.entity");
const district_entity_1 = require("./district.entity");
let Address = class Address {
};
__decorate([
    graphql_1.Field(() => String),
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Address.prototype, "id", void 0);
__decorate([
    graphql_1.Field(() => Number, { nullable: true }),
    typeorm_1.Column('int', { nullable: true }),
    __metadata("design:type", Number)
], Address.prototype, "number", void 0);
__decorate([
    graphql_1.Field(() => String),
    typeorm_1.Column(),
    __metadata("design:type", String)
], Address.prototype, "street", void 0);
__decorate([
    typeorm_1.ManyToOne(() => city_entity_1.City, { eager: true }),
    __metadata("design:type", district_entity_1.District)
], Address.prototype, "district", void 0);
__decorate([
    graphql_1.Field(() => String, { nullable: true }),
    typeorm_1.Column('text', { nullable: true }),
    __metadata("design:type", String)
], Address.prototype, "comment", void 0);
__decorate([
    graphql_1.Field(() => Date),
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], Address.prototype, "createdAt", void 0);
__decorate([
    graphql_1.Field(() => Date),
    typeorm_1.UpdateDateColumn(),
    __metadata("design:type", Date)
], Address.prototype, "updatedAt", void 0);
Address = __decorate([
    graphql_1.ObjectType(),
    typeorm_1.Entity()
], Address);
exports.Address = Address;
//# sourceMappingURL=address.entity.js.map