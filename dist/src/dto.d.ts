export declare class RegionIdInput {
    id: string;
}
export declare class CreateRegionInput {
    name: string;
}
export declare class UpdateRegionInput {
    name?: string;
}
export declare class CityIdInput {
    id: string;
}
export declare class CreateCityInput {
    name: string;
    region: RegionIdInput;
}
export declare class UpdateCityInput {
    name?: string;
    region?: RegionIdInput;
}
export declare class DistrictIdInput {
    id: string;
}
export declare class CreateDistrictInput {
    name: string;
    parent?: DistrictIdInput;
    city: CityIdInput;
}
export declare class UpdateDistrictInput {
    name?: string;
    parent?: DistrictIdInput;
    city?: CityIdInput;
}
export declare class CreateAddressInput {
    number?: number;
    street: string;
    comment?: string;
    district: DistrictIdInput;
}
export declare class UpdateAddressInput {
    number?: number;
    street?: string;
    comment?: string;
    district?: DistrictIdInput;
}
