"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AddressModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressModule = void 0;
const common_1 = require("@nestjs/common");
const address_resolver_1 = require("./resolver/address.resolver");
const city_resolver_1 = require("./resolver/city.resolver");
const district_resolver_1 = require("./resolver/district.resolver");
const region_resolver_1 = require("./resolver/region.resolver");
const address_service_1 = require("./service/address.service");
const city_service_1 = require("./service/city.service");
const district_service_1 = require("./service/district.service");
const region_service_1 = require("./service/region.service");
let AddressModule = AddressModule_1 = class AddressModule {
    static forRoot({ imports }) {
        return {
            module: AddressModule_1,
            imports,
            providers: [
                address_service_1.AddressService,
                city_service_1.CityService,
                district_service_1.DistrictService,
                region_service_1.RegionService,
                address_resolver_1.AddressResolver,
                city_resolver_1.CityResolver,
                district_resolver_1.DistrictResolver,
                region_resolver_1.RegionResolver,
            ],
            exports: [address_service_1.AddressService, city_service_1.CityService, district_service_1.DistrictService, region_service_1.RegionService],
        };
    }
};
AddressModule = AddressModule_1 = __decorate([
    common_1.Module({})
], AddressModule);
exports.AddressModule = AddressModule;
//# sourceMappingURL=address.module.js.map