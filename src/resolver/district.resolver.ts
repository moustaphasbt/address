import { Resolver } from '@nestjs/graphql';
import { BaseResolver } from '../common/base.resolver';
import { CreateDistrictInput, UpdateDistrictInput } from '../dto';
import { District } from '../entity/District.entity';
import { DistrictService } from '../service/district.service';

@Resolver(() => District)
export class DistrictResolver extends BaseResolver(
  District,
  CreateDistrictInput,
  UpdateDistrictInput,
  DistrictService,
) {}
