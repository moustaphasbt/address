"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DistrictService = void 0;
const common_1 = require("@nestjs/common");
const base_service_1 = require("../common/base.service");
const district_entity_1 = require("../entity/district.entity");
let DistrictService = class DistrictService extends base_service_1.BaseService(district_entity_1.District) {
};
DistrictService = __decorate([
    common_1.Injectable()
], DistrictService);
exports.DistrictService = DistrictService;
//# sourceMappingURL=district.service.js.map