import { Region } from './region.entity';
export declare class City {
    id: string;
    name: string;
    region: Region;
    createdAt: Date;
    updatedAt: Date;
}
