import { Resolver } from '@nestjs/graphql';
import { BaseResolver } from '../common/base.resolver';
import { CreateCityInput, UpdateCityInput } from '../dto';
import { City } from '../entity/city.entity';
import { CityService } from '../service/city.service';

@Resolver(() => City)
export class CityResolver extends BaseResolver(
  City,
  CreateCityInput,
  UpdateCityInput,
  CityService,
) {}
