import { Resolver } from '@nestjs/graphql';
import { BaseResolver } from '../common/base.resolver';
import { CreateAddressInput, UpdateAddressInput } from '../dto';
import { Address } from '../entity/address.entity';
import { AddressService } from '../service/address.service';

@Resolver(() => Address)
export class AddressResolver extends BaseResolver(
  Address,
  CreateAddressInput,
  UpdateAddressInput,
  AddressService,
) {}
