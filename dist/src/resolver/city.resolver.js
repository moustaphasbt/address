"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CityResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const base_resolver_1 = require("../common/base.resolver");
const dto_1 = require("../dto");
const city_entity_1 = require("../entity/city.entity");
const city_service_1 = require("../service/city.service");
let CityResolver = class CityResolver extends base_resolver_1.BaseResolver(city_entity_1.City, dto_1.CreateCityInput, dto_1.UpdateCityInput, city_service_1.CityService) {
};
CityResolver = __decorate([
    graphql_1.Resolver(() => city_entity_1.City)
], CityResolver);
exports.CityResolver = CityResolver;
//# sourceMappingURL=city.resolver.js.map