export declare class Region {
    id: string;
    name: string;
    createdAt: Date;
    updatedAt: Date;
}
