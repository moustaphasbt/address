import { District } from './district.entity';
export declare class Address {
    id: string;
    number?: number;
    street: string;
    district: District;
    comment?: string;
    createdAt: Date;
    updatedAt: Date;
}
