import { Injectable } from '@nestjs/common';
import { BaseService } from '../common/base.service';
import { District } from '../entity/district.entity';

@Injectable()
export class DistrictService extends BaseService(District) {}
