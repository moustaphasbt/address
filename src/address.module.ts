import { DynamicModule, Module } from '@nestjs/common';
import { AddressResolver } from './resolver/address.resolver';
import { CityResolver } from './resolver/city.resolver';
import { DistrictResolver } from './resolver/district.resolver';
import { RegionResolver } from './resolver/region.resolver';
import { AddressService } from './service/address.service';
import { CityService } from './service/city.service';
import { DistrictService } from './service/district.service';
import { RegionService } from './service/region.service';
import { AddressOptions } from './utils/common';

@Module({})
export class AddressModule {
  static forRoot({ imports }: Pick<AddressOptions, 'imports'>): DynamicModule {
    return {
      module: AddressModule,
      imports,
      providers: [
        AddressService,
        CityService,
        DistrictService,
        RegionService,
        AddressResolver,
        CityResolver,
        DistrictResolver,
        RegionResolver,
      ],
      exports: [AddressService, CityService, DistrictService, RegionService],
    };
  }
}
