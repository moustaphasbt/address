import { City } from './city.entity';
export declare class District {
    id: string;
    name: string;
    childrens?: District[];
    parent?: District;
    city: City;
    createdAt: Date;
    updatedAt: Date;
}
