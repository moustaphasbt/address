"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var District_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.District = void 0;
const graphql_1 = require("@nestjs/graphql");
const typeorm_1 = require("typeorm");
const city_entity_1 = require("./city.entity");
let District = District_1 = class District {
};
__decorate([
    graphql_1.Field(() => String),
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], District.prototype, "id", void 0);
__decorate([
    graphql_1.Field(() => String),
    typeorm_1.Column(),
    __metadata("design:type", String)
], District.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(() => District_1, (district) => district.childrens),
    __metadata("design:type", Array)
], District.prototype, "childrens", void 0);
__decorate([
    typeorm_1.ManyToOne(() => District_1),
    __metadata("design:type", District)
], District.prototype, "parent", void 0);
__decorate([
    typeorm_1.ManyToOne(() => city_entity_1.City),
    __metadata("design:type", city_entity_1.City)
], District.prototype, "city", void 0);
__decorate([
    graphql_1.Field(() => Date),
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], District.prototype, "createdAt", void 0);
__decorate([
    graphql_1.Field(() => Date),
    typeorm_1.UpdateDateColumn(),
    __metadata("design:type", Date)
], District.prototype, "updatedAt", void 0);
District = District_1 = __decorate([
    graphql_1.ObjectType(),
    typeorm_1.Entity()
], District);
exports.District = District;
//# sourceMappingURL=district.entity.js.map