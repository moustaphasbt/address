import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { City } from './city.entity';
import { District } from './district.entity';

@ObjectType()
@Entity()
export class Address {
  @Field(() => String)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field(() => Number, { nullable: true })
  @Column('int', { nullable: true })
  number?: number;

  @Field(() => String)
  @Column()
  street: string;

  @ManyToOne(() => City, { eager: true })
  district: District;

  @Field(() => String, { nullable: true })
  @Column('text', { nullable: true })
  comment?: string;

  @Field(() => Date)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => Date)
  @UpdateDateColumn()
  updatedAt: Date;
}
