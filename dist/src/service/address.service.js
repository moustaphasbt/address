"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressService = void 0;
const common_1 = require("@nestjs/common");
const base_service_1 = require("../common/base.service");
const address_entity_1 = require("../entity/address.entity");
let AddressService = class AddressService extends base_service_1.BaseService(address_entity_1.Address) {
};
AddressService = __decorate([
    common_1.Injectable()
], AddressService);
exports.AddressService = AddressService;
//# sourceMappingURL=address.service.js.map