import { Injectable } from '@nestjs/common';
import { BaseService } from '../common/base.service';
import { Address } from '../entity/address.entity';

@Injectable()
export class AddressService extends BaseService(Address) {}
