import { Resolver } from '@nestjs/graphql';
import { BaseResolver } from '../common/base.resolver';
import { CreateRegionInput, UpdateRegionInput } from '../dto';
import { Region } from '../entity/region.entity';
import { RegionService } from '../service/region.service';

@Resolver(() => Region)
export class RegionResolver extends BaseResolver(
  Region,
  CreateRegionInput,
  UpdateRegionInput,
  RegionService,
) {}
