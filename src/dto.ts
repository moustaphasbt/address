import { Field, InputType, Int } from '@nestjs/graphql';
import { IsNotEmpty, IsUUID } from 'class-validator';

@InputType()
export class RegionIdInput {
  @IsUUID()
  @IsNotEmpty()
  @Field(() => String)
  id: string;
}

@InputType()
export class CreateRegionInput {
  @Field(() => String)
  name: string;
}

@InputType()
export class UpdateRegionInput {
  @Field(() => String, { nullable: true })
  name?: string;
}

@InputType()
export class CityIdInput {
  @IsUUID()
  @IsNotEmpty()
  @Field(() => String)
  id: string;
}

@InputType()
export class CreateCityInput {
  @Field(() => String)
  name: string;

  @Field(() => RegionIdInput)
  region: RegionIdInput;
}

@InputType()
export class UpdateCityInput {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => RegionIdInput, { nullable: true })
  region?: RegionIdInput;
}

@InputType()
export class DistrictIdInput {
  @IsUUID()
  @IsNotEmpty()
  @Field(() => String)
  id: string;
}

@InputType()
export class CreateDistrictInput {
  @Field(() => String)
  name: string;

  @Field(() => DistrictIdInput, { nullable: true })
  parent?: DistrictIdInput;

  @Field(() => CityIdInput)
  city: CityIdInput;
}

@InputType()
export class UpdateDistrictInput {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => DistrictIdInput, { nullable: true })
  parent?: DistrictIdInput;

  @Field(() => CityIdInput, { nullable: true })
  city?: CityIdInput;
}

@InputType()
export class CreateAddressInput {
  @Field(() => Number, { nullable: true })
  number?: number;

  @Field(() => String)
  street: string;

  @Field(() => String, { nullable: true })
  comment?: string;

  @Field(() => DistrictIdInput)
  district: DistrictIdInput;
}

@InputType()
export class UpdateAddressInput {
  @Field(() => Number, { nullable: true })
  number?: number;

  @Field(() => String, { nullable: true })
  street?: string;

  @Field(() => String, { nullable: true })
  comment?: string;

  @Field(() => DistrictIdInput, { nullable: true })
  district?: DistrictIdInput;
}
