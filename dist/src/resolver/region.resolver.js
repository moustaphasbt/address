"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegionResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const base_resolver_1 = require("../common/base.resolver");
const dto_1 = require("../dto");
const region_entity_1 = require("../entity/region.entity");
const region_service_1 = require("../service/region.service");
let RegionResolver = class RegionResolver extends base_resolver_1.BaseResolver(region_entity_1.Region, dto_1.CreateRegionInput, dto_1.UpdateRegionInput, region_service_1.RegionService) {
};
RegionResolver = __decorate([
    graphql_1.Resolver(() => region_entity_1.Region)
], RegionResolver);
exports.RegionResolver = RegionResolver;
//# sourceMappingURL=region.resolver.js.map